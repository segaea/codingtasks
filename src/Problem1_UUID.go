package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main()  {
	// Read int
	var x int
	fmt.Print("# of UUID's to generate: ")
	_, _ = fmt.Scanf("%d", &x)
	for i := 0; i < x; i++ {
		rand.Seed(time.Now().UnixNano())
		b := make([]byte, 16)
		_, _ = rand.Read(b)
		fmt.Printf("%x-%x-%x-%x-%x\n", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
	}
}