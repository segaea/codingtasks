package main

import (
	"fmt"
	"math"
)

const Length = 16_000_000
const Count = 999_999

func main() {
	// Prime number <-> index of array
	prime  := make([]bool, Length)
	for i := 0; i < len(prime); i++ {
		prime[i] = true
	}
	// Update the multipliers of the current number as not Prime
	p := 2
	c := 0
	for ;c <= Count; {
		if(p - 1 == len(prime)) {
			prime = append(prime, )
		}
		if prime[p] {
			for i := 2 ; i * p < len(prime) ; i++ {
				prime[i * p] = false
			}
			c++
		}
		p++
	}
	fmt.Println(p-1)
	prime[2] = true
}

func PrimeNumberClassic() {
	start := 2
	for j := 1; j < Length; {
		start++
		isPrime := true
		for i := 2; i <= int(math.Sqrt(float64(start))); i++ {
			if start%i == 0 {
				isPrime = false
				break
			}
		}
		if isPrime {
			j++
		}
	}
	fmt.Println(start)
}
