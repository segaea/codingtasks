package main

import "fmt"

type node struct {
	number int
	next *node
}
type singleLinkedList struct {
	name        string
	head        *node
	currentNode *node
}

func createList(name string) *singleLinkedList {
	return &singleLinkedList{
		name: name,
	}
}

func (list *singleLinkedList) addNode(number int) error {
	s := &node{
		 number:   number,
	}
	if list.head == nil {
		list.head = s
	} else {
		currentNode := list.head
		for currentNode.next != nil {
			currentNode = currentNode.next
		}
		currentNode.next = s
	}
	return nil
}

func (p *singleLinkedList) DisplayList() error {
	currentNode := p.head
	if currentNode == nil {
		fmt.Println("Empty.")
		return nil
	}
	fmt.Printf("%+v\n", *currentNode)
	for currentNode.next != nil {
		currentNode = currentNode.next
		fmt.Printf("%+v\n", *currentNode)
	}
	return nil
}

func main()  {
	list := createList("myList")
	list.addNode(23)
	list.addNode(12)
	list.addNode(45)
	list.addNode(45)
	list.addNode(425)
	list.addNode(25)
	list.addNode(25)

	tempList := createList("tempList")
	// Remove duplicates by iterating and inserting into a secondary list
	fmt.Println("Original List : ")
	list.DisplayList()

	currentNode := list.head
	for currentNode.next != nil {
		tempNode := tempList.head
		duplicate := false
		for tempNode != nil {
			if(tempNode.number == currentNode.number) {
				duplicate = true
			}
			tempNode = tempNode.next
		}
		if(!duplicate) {
			tempList.addNode(currentNode.number)
		}
		currentNode = currentNode.next
		fmt.Printf("%+v\n", *currentNode)
	}
	fmt.Println("Removed duplicates List : ")

	tempList.DisplayList()
}



