package main

import (
	"fmt"
	"strconv"
)

func main() {
	inputString := "AABCCC"
	outputString := inputString
	rotation := 3
	fmt.Println("Input:     ", inputString)

	a := []rune(inputString)
	b := []rune(outputString)
	for i := 0; i < len(a); i++ {
		b[i] = a[i] + rune(rotation)
	}
	outputStringRLE := ""
	count := 1
	for i := 0; i < len(b); i++ {
		if (i+1) < len(b) && b[i] == b[i+1] {
			count++
			continue
		} else {
			if count == 1 {
				outputStringRLE = outputStringRLE + string(b[i])
			} else {
				outputStringRLE = outputStringRLE + string(b[i]) + strconv.Itoa(count)
			}
			count = 1
		}
	}
	fmt.Println("Output:    ", outputStringRLE)
}
